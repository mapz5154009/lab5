﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using WMPLib;
using mapz4;

namespace mapz4
{
    public interface IObserver
    {
        void Update(EventManager eManager);
    }

    public class EventManager
    {
        private  List<IObserver> observers = new List<IObserver>();

        public void Subscribe(IObserver observer)
        {
            observers.Add(observer);
        }
        public void UnSubscribe(IObserver observer)
        {
            observers.Remove(observer);
        }
        public void Notify()
        {
            foreach (IObserver observer in observers) 
            {
                observer.Update(this);
            }
        }
    }

    public class SoundReactionObserver   : IObserver
    {
        private string soundPath;
        private WindowsMediaPlayer mediaPlayer;

        public SoundReactionObserver(string soundPath)
        {
            this.soundPath = soundPath;
        }

        public void Update(EventManager eManager) 
        {
            if(!File.Exists(soundPath))
            {
                return;
            }

            mediaPlayer = new WindowsMediaPlayer();
            mediaPlayer.URL = soundPath;
            mediaPlayer.controls.play();
        }
    }
}
