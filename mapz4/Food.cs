﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace mapz4
{
    abstract class Food
    {
        public string textureUri { get; protected set; }
        public Point point { get; set; }

        public abstract Food Clone();

        public abstract void ApplyChanges(Snake snake);
    }

    class Apple : Food
    {
        public int lengthIncrease { get; protected set; }

        public Apple() 
        {
            lengthIncrease = 1;
            textureUri = "C:\\Users\\Ноутбук\\Desktop\\mapz4\\lab4\\mapz4\\images\\apple.png";
        }

        public override Food Clone()
        {
            Apple clonedFood = new Apple();
            clonedFood.lengthIncrease = lengthIncrease;
            clonedFood.textureUri = textureUri;

            return clonedFood;
        }

        public override void ApplyChanges(Snake snake) 
        {
            // Snake grows by 1
            snake.Grow();
        }
    }

    class GoldenApple : Food
    {
        public int lengthIncrease { get; protected set; }
        public int armorDuration { get; protected set; }

        public GoldenApple()
        {
            lengthIncrease = 3;
            armorDuration = 5;
            textureUri = "C:\\Users\\Ноутбук\\Desktop\\mapz4\\lab4\\mapz4\\images\\goldenapple.png";
        }

        public override Food Clone()
        {
            GoldenApple clonedFood = new GoldenApple();
            clonedFood.lengthIncrease = lengthIncrease;
            clonedFood.armorDuration = armorDuration;
            clonedFood.textureUri = textureUri;

            return clonedFood;
        }

        public override void ApplyChanges(Snake snake)
        {
            // Snake grows by 3 and gets armor for 5 seconds
            snake.Armor = true;
            for (int i = 0; i < lengthIncrease; i++) 
            {
                snake.Grow();
            }
        }
    }

    class WaterMelon : Food
    {
        public int speedMultiplier { get; protected set; }
        public int speedMultiplierDuration { get; protected set; }

        public WaterMelon()
        {
            speedMultiplier = 2;
            speedMultiplierDuration = 10;
            textureUri = "C:\\Users\\Ноутбук\\Desktop\\mapz4\\lab4\\mapz4\\images\\watermelon.png";
        }

        public override Food Clone()
        {
            WaterMelon clonedFood = new WaterMelon();
            clonedFood.speedMultiplier = speedMultiplier;
            clonedFood.speedMultiplierDuration = speedMultiplierDuration;
            clonedFood.textureUri = textureUri;

            return clonedFood;
        }

        public override void ApplyChanges(Snake snake)
        {
            // Snakes speed multiples by 2 for 10 seconds
            snake.Speed = snake.Speed * speedMultiplier;
        }
    }
}
