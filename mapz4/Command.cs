﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mapz4;
using System.Windows;

namespace mapz4
{
    public interface ICommand
    {
        void Execute();
    }

    public class ChangeDirectionCommand : ICommand
    {
        private readonly Snake _snake;
        private readonly Direction _direction;

        public ChangeDirectionCommand(Snake snake, Direction direction)
        {
            _snake = snake;
            _direction = direction;
        }

        public void Execute()
        {
            _snake.ChangeDirection(_direction);
        }
    }
}

