﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mapz4
{
    public partial class MainWindow : Window
    {
        private GameFacadeProxy gameFacadeProxy;
        private EventManager spawnSnakePublisher = new EventManager();
        private EventManager gameStartPublisher = new EventManager();
        private IFoodSpawnStrategy foodSpawnStrategy = new RandomFoodSpawnStrategy();

        public MainWindow()
        {
            InitializeComponent();

            gameFacadeProxy = new GameFacadeProxy(new GameFacade());
            spawnSnakePublisher.Subscribe(new SoundReactionObserver("C:\\Users\\Ноутбук\\Desktop\\sir.mp3"));
            gameStartPublisher.Subscribe(new SoundReactionObserver("C:\\Users\\Ноутбук\\Desktop\\cr.mp3"));
        }

        private void CreateCanvasButton_Click(object sender, RoutedEventArgs e)
        {
            gameFacadeProxy.InitializeCanvas(GameCanvas);
            gameStartPublisher.Notify();
        }

        private void DeleteCanvasButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void AquaThemeButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton selectedRadioButton = RadioButtons.Children.OfType<RadioButton>()
                .FirstOrDefault(r => r.IsChecked == true);

            string gameMode = selectedRadioButton != null ? selectedRadioButton.Content.ToString() : "Режим не обрано!";
            bool obs = ObstaclesCheckBox.IsChecked.Value;

            gameFacadeProxy.CreateGame(gameMode, new AquaThemeFactory(), obs, foodSpawnStrategy.GetFoodPoints());
        }

        private void FireThemeButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton selectedRadioButton = RadioButtons.Children.OfType<RadioButton>()
                .FirstOrDefault(r => r.IsChecked == true);

            string gameMode = selectedRadioButton != null ? selectedRadioButton.Content.ToString() : "Режим не обрано!";
            bool obs = ObstaclesCheckBox.IsChecked.Value;

            gameFacadeProxy.CreateGame(gameMode, new FireThemeFactory(), obs, foodSpawnStrategy.GetFoodPoints());
        }

        private void GameModeButton_Click(object sender, RoutedEventArgs e)
        {
            gameFacadeProxy.ShowGameMode();
        }

        private void AddSnakeButton_Click(object sender, RoutedEventArgs e)
        {
            string name = (EnterNameBox.Text == "") ? "Chinazes" : EnterNameBox.Text;

            if(gameFacadeProxy.AddSnake(name))
                spawnSnakePublisher.Notify();

            List<Snake> snakelist = gameFacadeProxy.GetSnakeList();

            ButtonUp.Tag = new ChangeDirectionCommand(snakelist[0], Direction.Up);
            ButtonDown.Tag = new ChangeDirectionCommand(snakelist[0], Direction.Down);
            ButtonLeft.Tag = new ChangeDirectionCommand(snakelist[0], Direction.Left);
            ButtonRight.Tag = new ChangeDirectionCommand(snakelist[0], Direction.Right);
        }

        private void BonusButton_Click(object sender, RoutedEventArgs e)
        {
            IBonus bonus = new Bonus();

            if (additionalLifeCheckbox.IsChecked == true)
            {
                bonus = new AdditionalLifeDecorator(bonus);
            }

            if (randomTeleportCheckbox.IsChecked == true)
            {
                bonus = new RandomTeleportDecorator(bonus);
            }

            gameFacadeProxy.ApplyBonus(bonus);
        }

        private void ButtonChangeDirection_Click(object sender, RoutedEventArgs e)
        {
            var command = (sender as Button).Tag as ICommand;
            command.Execute();
        }

        private void SetSpawnStrategy(IFoodSpawnStrategy strategy)
        {
            foodSpawnStrategy = strategy;
        }

        private void ButtonRandomFood_Click(object sender, RoutedEventArgs e)
        {
            SetSpawnStrategy(new RandomFoodSpawnStrategy());
        }

        private void ButtonRowFood_Click(object sender, RoutedEventArgs e)
        {
            SetSpawnStrategy(new RowFoodSpawnStrategy());
        }
    }
}
